from sqlite3 import Timestamp
from django.db import models

class Tag(models.Model):
    Id              = models.AutoField(primary_key = True)
    Title           = models.CharField(max_length=253,unique=True)
    CreatedDate     = models.DateTimeField( null = True , blank=True, auto_now_add=True)
    class Meta:
        db_table = 'tbl_tag'




class Snippets(models.Model):
    Id                              = models.AutoField(primary_key=True)
    TagId                           = models.ForeignKey(Tag, on_delete=models.CASCADE, related_name= 'Tag_Related_Id_Field')
    Title                           = models.CharField(max_length=253)
    Timestamp                       = models.DateTimeField( null = True , blank=True, auto_now_add=True)                
    Snippets                        = models.TextField(blank=True)
    class Meta:
        db_table = 'tbl_Snippets'