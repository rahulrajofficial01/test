from django.urls import path,re_path
from .views import *

urlpatterns = [
    re_path(r'^CreateTags/$', ManageTags.CreateTags),
    re_path(r'UpdateTags/$', ManageTags.UpdateTags),
    re_path(r'^ListAllTags/$', ManageTags.ListAllTags),
    re_path(r'^GetTagsDetailById/$', ManageTags.GetTagsDetailById),

    re_path(r'^CreateSnippets/$', ManageSnippets.CreateSnippets),
    re_path(r'UpdateSnippets/$', ManageSnippets.UpdateSnippets),
    re_path(r'^ListAllSnippets/$', ManageSnippets.ListAllSnippets),
    re_path(r'^GetSnippetsDetailById/$', ManageSnippets.GetSnippetsDetailById),
    re_path(r'^DeleteSnippets/$', ManageSnippets.DeleteSnippets),

    
]




