from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated,IsAuthenticated
from rest_framework.decorators import permission_classes
from django.contrib.auth.models import User
from rest_framework.decorators import api_view
from rest_framework import status
from .models import *
from .serializers import *

def ResponceObject(msg,success,data,count):
    response_data = {}
    response_data['Message'] = msg
    response_data['Success'] = success
    response_data['TotalCount'] = count
    response_data['Data'] = data
    return(response_data)



class ManageTags(APIView):
    
    @api_view(['POST'])
    @permission_classes([IsAuthenticated, ])
    def CreateTags(request):
        try:
            if request.method == "POST":
                serializer = TagSerializer(data=request.data)
                if serializer.is_valid():
                    serializer.save()
                    return Response(data = ResponceObject('Operation Successfull Tag Created !',True,serializer.data,1),status= status.HTTP_200_OK)
                else:
                    return Response(data = ResponceObject('Operation Failed!',True,serializer.errors,1),status= status.HTTP_200_OK)
        except Exception as e:
            print(e)
            return Response(data = ResponceObject('Something went wrong',False,str(e),0), status= status.HTTP_200_OK)


    @api_view(['POST'])
    @permission_classes([IsAuthenticated, ])
    def UpdateTags(request):
        try:
            if request.method == "POST":
                Id = request.data.get("Id")
                ResultData = Tag.objects.get(Id=Id)
                serializer = TagSerializer(ResultData,data = request.data)
                if serializer.is_valid(): 
                    serializer.save()
                    return Response(data = ResponceObject('Operation Successfull Tag Updated !',True,serializer.data,1),status= status.HTTP_200_OK)
                else:
                    return Response(data = ResponceObject('Operation Failed!',True,serializer.errors,1),status= status.HTTP_200_OK)
        except Exception as e:
            print(e)
            return Response(data = ResponceObject('Something went wrong',False,str(e),0), status= status.HTTP_200_OK)


    @api_view(['POST'])
    @permission_classes([IsAuthenticated, ])
    def ListAllTags(request):
        try:
            if request.method == "POST":
                ResultData = Tag.objects.all()
                serializer = TagSerializer(ResultData,many=True)
                return Response(data = ResponceObject('Operation Successfull',True,serializer.data,ResultData.count()),status= status.HTTP_200_OK)
        except Exception as e:
            print(e)
            return Response(data = ResponceObject('Something went wrong',False,str(e),0), status= status.HTTP_200_OK)


    @api_view(['POST'])
    @permission_classes([IsAuthenticated, ])
    def GetTagsDetailById(request):
        try:
            if request.method == "POST":
                Id = request.data.get("Id")
                ResultData = Tag.objects.get(Id=Id)
                serializer = TagSerializer(ResultData)
                return Response(data = ResponceObject('Operation Successfull',True,serializer.data,1),status= status.HTTP_200_OK)
        except Exception as e:
            print(e)
            return Response(data = ResponceObject('Something went wrong',False,str(e),0), status= status.HTTP_200_OK)




class ManageSnippets(APIView):
    
    @api_view(['POST'])
    @permission_classes([IsAuthenticated, ])
    def CreateSnippets(request):
        try:
            if request.method == "POST":
                Title = request.data.get("Title")
                serializer = SnippetsSerializer(data=request.data)
                try:
                    Check = Tag.objects.get(Title=Title)
                except Tag.DoesNotExist:
                    if serializer.is_valid():
                        RefId = serializer.save()
                        Tag.objects.create(Title=Title)
                        TagId = Tag.objects.latest('Id')
                        Editdata = Snippets.objects.get(Id=RefId.Id)
                        Editdata.Title = Title
                        Editdata.TagId = TagId
                        Editdata.save()
                        serializers = SnippetsSerializer(Editdata)
                        return Response(data = ResponceObject('Operation Successfull Tag Created !',True,serializers.data,1),status= status.HTTP_200_OK)
                    else:
                        return Response(data = ResponceObject('Operation Failed!',True,serializer.errors,1),status= status.HTTP_200_OK)
                if serializer.is_valid():
                    serializer.save()
                    return Response(data = ResponceObject('Operation Successfull Tag Created !',True,serializer.data,1),status= status.HTTP_200_OK)
                else:
                    return Response(data = ResponceObject('Operation Failed!',True,serializer.errors,1),status= status.HTTP_200_OK)    
        except Exception as e:
            print(e)
            return Response(data = ResponceObject('Something went wrong',False,str(e),0), status= status.HTTP_200_OK)


    @api_view(['POST'])
    @permission_classes([IsAuthenticated, ])
    def UpdateSnippets(request):
        try:
            if request.method == "POST":
                Id = request.data.get("Id")
                ResultData = Snippets.objects.get(Id=Id)
                serializer = SnippetsSerializer(ResultData,data = request.data)
                if serializer.is_valid(): 
                    serializer.save()
                    return Response(data = ResponceObject('Operation Successfull Tag Updated !',True,serializer.data,1),status= status.HTTP_200_OK)
                else:
                    return Response(data = ResponceObject('Operation Failed!',True,serializer.errors,1),status= status.HTTP_200_OK)
        except Exception as e:
            print(e)
            return Response(data = ResponceObject('Something went wrong',False,str(e),0), status= status.HTTP_200_OK)


    @api_view(['POST'])
    @permission_classes([IsAuthenticated, ])
    def ListAllSnippets(request):
        try:
            if request.method == "POST":
                ResultData = Snippets.objects.all().order_by('-Id')
                serializer = SnippetsSerializer(ResultData, many=True)
                return Response(data = ResponceObject('Operation Successfull',True,serializer.data,ResultData.count()),status= status.HTTP_200_OK)
        except Exception as e:
            print(e)
            return Response(data = ResponceObject('Something went wrong',False,str(e),0), status= status.HTTP_200_OK)


    @api_view(['POST'])
    @permission_classes([IsAuthenticated, ])
    def GetSnippetsDetailById(request):
        try:
            if request.method == "POST":
                Id = request.data.get("Id")
                ResultData = Snippets.objects.get(Id=Id)
                serializer = SnippetsSerializer(ResultData)
                return Response(data = ResponceObject('Operation Successfull',True,serializer.data,1),status= status.HTTP_200_OK)
        except Exception as e:
            print(e)
            return Response(data = ResponceObject('Something went wrong',False,str(e),0), status= status.HTTP_200_OK)


    @api_view(['POST'])
    @permission_classes([IsAuthenticated, ])
    def DeleteSnippets(request):
        try:
            if request.method == "POST":
                Id = request.data.get("Id")
                ResultData = Snippets.objects.filter(Id=Id).delete()
                ResultDatas = Snippets.objects.all().order_by('-Id')
                serializer = SnippetsSerializer(ResultDatas, many=True)
                return Response(data = ResponceObject('Operation Successfull',True,serializer.data,ResultDatas.count()),status= status.HTTP_200_OK)
        except Exception as e:
            print(e)
            return Response(data = ResponceObject('Something went wrong',False,str(e),0), status= status.HTTP_200_OK)
