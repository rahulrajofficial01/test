from rest_framework import serializers
from .models import *



class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model  = Tag
        fields = '__all__'

class SnippetsSerializer(serializers.ModelSerializer):
    class Meta:
        model  = Snippets
        fields = '__all__'