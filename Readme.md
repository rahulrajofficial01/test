####################################################################
############################ Procedure #############################
####################################################################


Create User through Admin portal : Login in as Super Admin and create Users : http://127.0.0.1:8000/admin

Use the below link for login and toekn generation
http://127.0.0.1:8000/api/token/

{
    "username": "NoData",
    "password": "123"
}

To get a new access token, you should use the refresh token endpoint /api/token/refresh/ posting the refresh token:

http://127.0.0.1:8000/api/token/refresh/

{
    "refresh":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTY0NzE1NzEzNSwiaWF0IjoxNjQ3MDcwNz1LCJqdGkiOiIwNjRmNzgyMWRlZDA0NTRhODQ0ZjcwNzBlYWZiYmM2YSIsInVzZXJfaWQiOjJ9agb_ODBa63uWlqZXIuLGE0ZfDDTVs4b2N8NL4gwK538"
}

#Tag creation api

http://127.0.0.1:8000/api/CreateTags/

{
    "Title": "Example"
}

#Update Tags

http://127.0.0.1:8000/api/UpdateTags/


{
    "Id": 1,
    "Title": "Examples"
}

#Listing all tags

http://127.0.0.1:8000/api/ListAllTags/

#Get Tag details

http://127.0.0.1:8000/api/GetTagsDetailById/

{
    "Id": 1,
}


#Create Snippets

http://127.0.0.1:8000/api/CreateSnippets/

{
    "TagId": 1,
    "Title": "Examples",
    "Snippets": "Testing"
}

#Update Snippets

http://127.0.0.1:8000/api/UpdateSnippets/



{
    "Id": 12,
    "TagId": 9,
    "Title": "Test",
    "Snippets": "Testing"
}

#Listing All Snippets

http://127.0.0.1:8000/api/ListAllSnippets/

#Get Snippets Details

http://127.0.0.1:8000/api/GetSnippetsDetailById/


{
    "Id": 1,
}

#Delete Snippets and Listing

http://127.0.0.1:8000/api/DeleteSnippets/

{
    "Id": 1,
}
